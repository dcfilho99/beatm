INSERT IGNORE INTO genero_musical(id, version, nome) VALUES(1, 0, 'Rap');
INSERT IGNORE INTO genero_musical(id, version, nome) VALUES(2, 0, 'Hip-Hop');
INSERT IGNORE INTO genero_musical(id, version, nome) VALUES(3, 0, 'Funk');

INSERT IGNORE INTO pais (id, nome, sigla, version) VALUES
(1, 'Brasil', 'BR', 0),
(2, 'Estados Unidos da América', 'EUA', 0),
(3, 'França', 'FRA', 0),
(4, 'Espanha', 'ESP', 0),
(5, 'Alemanha', 'ALE', 0),
(6, 'Canadá', 'CAN', 0),
(7, 'Portugal', 'PT', 0),
(8, 'Japão', 'JAP', 0);


INSERT IGNORE INTO estado(id, nome, sigla, pais_id) VALUES 
(1, 'Acre', 'AC', 1),
(2, 'Alagoas', 'AL', 1),
(3, 'Amazonas', 'AM', 1),
(4, 'Amapá', 'AP', 1),
(5, 'Bahia', 'BA', 1),
(6, 'Ceará', 'CE', 1),
(7, 'Distrito Federal', 'DF', 1),
(8, 'Espírito Santo', 'ES', 1),
(9, 'Goiás', 'GO', 1),
(10, 'Maranhão', 'MA', 1),
(11, 'Minas Gerais', 'MG', 1),
(12, 'Mato Grosso do Sul', 'MS', 1),
(13, 'Mato Grosso', 'MT', 1),
(14, 'Pará', 'PA', 1),
(15, 'Paraíba', 'PB', 1),
(16, 'Pernambuco', 'PE', 1),
(17, 'Piauí', 'PI', 1),
(18, 'Paraná', 'PR', 1),
(19, 'Rio de Janeiro', 'RJ', 1),
(20, 'Rio Grande do Norte', 'RN', 1),
(21, 'Rondônia', 'RO', 1),
(22, 'Roraima', 'RR', 1),
(23, 'Rio Grande do Sul', 'RS', 1),
(24, 'Santa Catarina', 'SC', 1),
(25, 'Sergipe', 'SE', 1),
(26, 'São Paulo', 'SP', 1),
(27, 'Tocantins', 'TO', 1);

INSERT IGNORE INTO cidade(id, version, nome, estado_id) VALUES
(2878, 0, 'Curitiba', 18);

INSERT IGNORE INTO usuario (id, email, senha, tipo, dt_hora_cadastro, is_ativo, version) 
VALUES(1, 'teste@teste.com.br', md5('teste'), 'ARTISTA', now(), 1, 0);

INSERT IGNORE INTO artista (id, nome, telefone1, dt_nascimento, cidade_id) 
VALUES(1, 'daniel filho', '(41)99757-0675', '1990-01-30', 2878);