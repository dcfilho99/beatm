package br.beatm.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.beatm.dto.AuthRequestDTO;
import br.beatm.entity.Usuario;
import br.beatm.exception.ServiceException;
import br.beatm.repository.UsuarioRepository;
import br.beatm.util.CriptografiaUtil;

@Service("loginService")
public class LoginService {

	@Autowired
	private UsuarioRepository repo;
	
	@Autowired
	private UsuarioService usuarioService;
	
	public LoginService() {
	
	}

	public Usuario getUsuarioLogin(AuthRequestDTO usuario) throws ServiceException {
		usuario.setPassword(CriptografiaUtil.md5(usuario.getPassword()));
		Usuario usu = this.repo.getUsuarioLogin(usuario.getEmail(), usuario.getPassword());
		return usu;
	}

	/*public UsuarioLogadoDTO me(String email) {
		UsuarioLogadoDTO dto = new UsuarioLogadoDTO();
		dto.setUsuario(this.usuarioService.getUsuarioByEmail(email));
		dto.getUsuario().setSenha(null);
		dto.setGuiche(null);
		dto.setPosicaoPropria(this.ppService.getPosicaoPropriaAtual(dto.getUsuario().getFuncionario().getEmpresa()));
		dto.setDtHrAcesso(new Date());
		return dto;
	}
	
	public InstanciarUsuarioJsonDTO meJson(String email) {
		Usuario usuario = this.usuarioService.getUsuarioByEmail(email);
		
		InstanciarUsuarioJsonDTO dto = new InstanciarUsuarioJsonDTO();
		
		dto.setIdUsuario(usuario.getId());
		dto.setNome(usuario.getFuncionario().getPessoaFisica().getPessoa().getNome());
		dto.setCpf(usuario.getFuncionario().getPessoaFisica().getPessoa().getCpfCnpj());
		
		dto.setIdCargo(usuario.getFuncionario().getCargo().getId());
		dto.setCargo(usuario.getFuncionario().getCargo().getNome());
		
		dto.setIdSetor(usuario.getFuncionario().getCargo().getSetor().getId());
		dto.setSetor(usuario.getFuncionario().getCargo().getSetor().getNome());
		
		dto.setIdEmpresaLicenciada(usuario.getFuncionario().getPessoaFisica().getPessoa().getEmpresa().getId());
		dto.setEmpresaLicenciada(usuario.getFuncionario().getPessoaFisica().getPessoa().getEmpresa().getNomeFantasia());
		
		return dto;
	}*/
}
