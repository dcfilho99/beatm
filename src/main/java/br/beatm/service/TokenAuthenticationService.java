package br.beatm.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import br.beatm.util.ToolsUtil;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenAuthenticationService {
	
	// EXPIRATION_TIME = 1 dia
	static final Integer EXPIRATION_TIME = 2;
	static final String SECRET = "CyraxSecretHash";
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";
	
	public static void addAuthentication(HttpServletResponse response, String username, boolean isRememberMe) {
		DateTime agora = new DateTime();
		agora = agora.plusMinutes(EXPIRATION_TIME);
		
		String JWT;
		if (!isRememberMe) {
			JWT = Jwts.builder()
					.setSubject(username)
					.setExpiration(agora.toDate())
					.signWith(SignatureAlgorithm.HS512, SECRET)
					.compact();
		} else {
			JWT = Jwts.builder()
					.setSubject(username)
					.signWith(SignatureAlgorithm.HS512, SECRET)
					.compact();
		}
		
		
		response.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
	}
	
	public static Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		if (token != null) {
			String user = Jwts.parser()
					.setSigningKey(SECRET)
					.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
					.getBody().getSubject();
			
			if (user != null) {
				return new UsernamePasswordAuthenticationToken(user, null, null);
			}
		}
		return null;
	}
	
	public static boolean isTokenValido(HttpServletRequest request) {
		Authentication auth = getAuthentication(request);
		if (!ToolsUtil.isNull(auth)) {
			return auth.isAuthenticated();
		}
		return Boolean.FALSE;
	}
	
	public static void clear(HttpServletResponse response) {
		response.setHeader(HEADER_STRING, null);
	}
	
}