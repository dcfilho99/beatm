package br.beatm.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.beatm.entity.ArtistaFoto;
import br.beatm.entity.Usuario;
import br.beatm.repository.ArtistaFotoRepository;
import br.beatm.repository.UsuarioRepository;
import br.beatm.util.FotoUtil;
import br.beatm.util.ToolsUtil;

@Transactional
@Service("usuarioService")
public class UsuarioService {

	@Autowired
	private UsuarioRepository repo;
	
	@Autowired
	private ArtistaFotoRepository artistaFotoRepository;
	
	public Usuario getUsuarioByEmail(String email) {
		Usuario usu = this.repo.findByEmail(email);
		loadLazyProperties(usu);
		return usu;
	}

	private void loadLazyProperties(Usuario usu) {
		if (!ToolsUtil.isNull(usu)) {
			/*Hibernate.initialize(usu.getGuiches());
			Hibernate.initialize(usu.getResources());*/
		}
	}
	
	public Usuario ativarDesativar(Usuario usuario) {
		Usuario base = this.repo.findById(usuario.getId()).get();
		
		base.setIsAtivo(usuario.getIsAtivo());
		
		base = this.repo.save(base);
		this.loadLazyProperties(base);
		return base;
	}
	
	public byte[] getFotoUsuario(Usuario usuario, HttpServletRequest request) {
		ArtistaFoto foto = this.artistaFotoRepository.findByArtista(usuario);
		if (!ToolsUtil.isNull(foto)) {
			return foto.getConteudo();
		}
		byte[] fotoDefault = FotoUtil.getFotoUserDefault(request);
		return fotoDefault;
	}
}
