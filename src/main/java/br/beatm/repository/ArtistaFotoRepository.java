package br.beatm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.beatm.entity.ArtistaFoto;
import br.beatm.entity.Usuario;

public interface ArtistaFotoRepository extends JpaRepository<ArtistaFoto, Long> {

	ArtistaFoto findByArtista(Usuario usuario);
}
