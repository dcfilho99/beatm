package br.beatm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.beatm.entity.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	@Query(value = " select usu from Usuario usu where usu.email = :email and senha = :senha ")
	Usuario getUsuarioLogin(@Param("email") String email, @Param("senha") String senha);
	
	Usuario findByEmail(@Param("email") String email);
	
	/*@Query(value = " select " + 
				   "	usu.id as id, " + 
				   " 	usu.email as email, " + 
				   " 	pessoa.nome as nome, " + 
				   " 	CASE WHEN usu.isAtivo=1 THEN 'Ativo' ELSE 'Inativo' END as status, " + 
				   " 	role.nome as grupo " + 
				   " from Usuario usu " + 
				   " inner join usu.funcionario func " + 
				   " inner join func.pessoaFisica pf " + 
				   " inner join pf.pessoa pessoa " + 
				   " inner join func.empresa empresa " +  
				   " left join usu.role role " + 
				   " where empresa.id = :idEmpresa " + 
				   " and ( (:email IS NULL OR :email = '') OR LOWER(usu.email) = LOWER(:email) )" + 
				   " and ( :nome IS NULL OR LOWER(pessoa.nome) LIKE CONCAT('%', LOWER(:nome) ,'%')) " + 
				   " and ( :isAtivo IS NULL OR usu.isAtivo = :isAtivo) " + 
				   " order by usu.email asc, pessoa.nome asc ")		
	Page<UsuarioCustom> consultar(@Param("email") String email, 
									@Param("nome") String nome, 
									@Param("isAtivo") Boolean isAtivo, 
									@Param("idEmpresa") Integer idEmpresa,
									Pageable page);
	
	
	List<Usuario> findByEmpresaIdOrderByEmailAsc(Integer idEmpresa);*/
}
