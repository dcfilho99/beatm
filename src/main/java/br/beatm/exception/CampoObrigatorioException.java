package br.beatm.exception;

public class CampoObrigatorioException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CampoObrigatorioException(Exception e) {
		super(e);
	}

	public CampoObrigatorioException(String message) {
		super(message);
	}
}
