package br.beatm.exception;

public class PermissaoAcaoException extends Exception {

	private static final long serialVersionUID = 1L;

	public PermissaoAcaoException(Exception e) {
		super(e);
	}

	public PermissaoAcaoException(String message) {
		super(message);
	}
}
