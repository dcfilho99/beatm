package br.beatm.util;

import java.math.BigDecimal;
import java.text.NumberFormat;

public abstract class NumberUtil {

	private static NumberFormat nf = NumberFormat.getCurrencyInstance();
	
	public static String formatNumber(Double number) {
		return nf.format(number);
	}
	
	public static String formatNumber(BigDecimal number) {
		return nf.format(number);
	}
	
	public static String formatNumber(Float number) {
		return nf.format(number);
	}
}
