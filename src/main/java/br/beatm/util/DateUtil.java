package br.beatm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;

import br.beatm.exception.ServiceException;

public abstract class DateUtil {

	public static Date parse(String dateAsString, String format) throws ServiceException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(dateAsString);
		} catch (ParseException e) {
			throw new ServiceException("Data inválida: " + dateAsString);
		}
	}
	
	public static String format(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	public static boolean isAfter(Date index, Date offset) {
		DateTime ind = new DateTime(index);
		DateTime of = new DateTime(offset);
		return ind.isAfter(of.getMillis());
	}
	
	public static boolean isBefore(Date index, Date offset) {
		DateTime ind = new DateTime(index);
		DateTime off = new DateTime(offset);
		return ind.isBefore(off.getMillis());
	}
}
