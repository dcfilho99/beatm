package br.beatm.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.beatm.exception.ServiceException;

public abstract class CriptografiaUtil {

	public static String md5(String str) throws ServiceException {
		if (ToolsUtil.isNull(str)) {
			return null;
		}
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new ServiceException(e);
		}
		BigInteger hash = new BigInteger(1, md.digest(str.getBytes()));
		str = hash.toString(16);			
		return str;
	}
	
}
