package br.beatm.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;

public abstract class FotoUtil {

	public static byte[] getFotoUserDefault(HttpServletRequest request) {
		String contextPath = request.getServletContext().getRealPath("/assets/img/user-default.png");
		try {
			return Files.readAllBytes(Paths.get(contextPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
