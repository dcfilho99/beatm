package br.beatm.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class ToolsUtil {

	public static boolean isNull(Object obj) {
		return obj == null;
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isNull(List list) {
		return (list == null) || (list.size() == 0);
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isNull(Set list) {
		return (list == null) || (list.size() == 0);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List arrayToList(Object[] array) {
		List retorno = new ArrayList<>();
		if (!ToolsUtil.isNull(array)) {
			for (Object obj : array) {
				retorno.add(obj);
			}
		}
		return retorno;
	}
}
