package br.beatm.util;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

public abstract class JsonUtil {

	public static String toJSON(Object objeto) {
		Gson gson = new Gson();
		return gson.toJson(objeto);
	}
	
	public static Object fromJSON(String objectAsString, Class<?> clazz) {
		Gson gson = new Gson();
		return gson.fromJson(objectAsString, clazz);
	}
	
	public static <T> List<T> fromJSONList(String objectAsString, Class<T> clazz) {
		try{
	        Gson gson = new Gson();
	        List<T> listObjects = new ArrayList<>();
	        
	        JSONArray jsonArray = new JSONArray(objectAsString);
	        for(int i=0; i<jsonArray.length(); i++){
	        	JSONObject jsonObject = jsonArray.getJSONObject(i);

	            listObjects.add(gson.fromJson(jsonObject.toString(), clazz));
	        }
	        return listObjects;
		}catch(Exception e){
	        e.printStackTrace();
	    }
		return null;
	}
}
