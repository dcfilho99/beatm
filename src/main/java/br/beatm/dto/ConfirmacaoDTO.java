package br.beatm.dto;

public class ConfirmacaoDTO {

	private Integer id;
	private String mensagem;
	private boolean isConfirma;

	public ConfirmacaoDTO() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public boolean getIsConfirma() {
		return isConfirma;
	}

	public void setIsConfirma(boolean isConfirma) {
		this.isConfirma = isConfirma;
	}

}
