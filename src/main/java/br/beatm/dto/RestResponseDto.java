package br.beatm.dto;

import java.util.List;

public class RestResponseDto {

	private boolean success;
	private String mensagem;
	private Object data;
	private List<ConfirmacaoDTO> confirmacoes;
	private int code;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public List<ConfirmacaoDTO> getConfirmacoes() {
		return confirmacoes;
	}

	public void setConfirmacoes(List<ConfirmacaoDTO> confirmacoes) {
		this.confirmacoes = confirmacoes;
	}

}
