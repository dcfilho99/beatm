package br.beatm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.beatm.entity.Usuario;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioLogadoDTO {

	private Usuario usuario;
	private String foto;

	public UsuarioLogadoDTO() {

	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	

}
