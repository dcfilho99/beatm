package br.beatm.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import br.beatm.entity.types.TpPrivacidadeType;

@Entity
@Table(name = "album")
@Audited(withModifiedFlag = true)
public class Album implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Version
	private Long version;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "artista_id")
	private Artista artista;

	@NotNull
	private String titulo;

	private String descricao;

	@Column(name = "dt_criacao")
	private Date dtCriacao;

	@OneToOne(mappedBy = "album")
	private AlbumFoto foto;

	@NotNull
	@Enumerated(EnumType.STRING)
	private TpPrivacidadeType privacidade;

	@OneToMany(mappedBy = "album", fetch = FetchType.LAZY)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private List<Faixa> faixas = new ArrayList<>();
	
	@OneToMany(mappedBy = "album", fetch = FetchType.LAZY)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private List<LikeAlbum> likes = new ArrayList<>();

	public Album() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Artista getArtista() {
		return artista;
	}

	public void setArtista(Artista artista) {
		this.artista = artista;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public TpPrivacidadeType getPrivacidade() {
		return privacidade;
	}

	public void setPrivacidade(TpPrivacidadeType privacidade) {
		this.privacidade = privacidade;
	}

	public AlbumFoto getFoto() {
		return foto;
	}

	public void setFoto(AlbumFoto foto) {
		this.foto = foto;
	}

	public List<Faixa> getFaixas() {
		return faixas;
	}

	public void setFaixas(List<Faixa> faixas) {
		this.faixas = faixas;
	}

	public List<LikeAlbum> getLikes() {
		return likes;
	}

	public void setLikes(List<LikeAlbum> likes) {
		this.likes = likes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Album other = (Album) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ArtistaAlbum [id=" + id + "]";
	}

}
