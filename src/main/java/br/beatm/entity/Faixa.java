package br.beatm.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

@Entity
@Table(name = "faixa")
@Audited(withModifiedFlag = true)
public class Faixa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Version
	private Long version;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "artista_id")
	private Artista artista;

	@NotNull
	private String nome;

	@NotNull
	private String duracao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "album_id")
	private Album album;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "genero_musical_id")
	private GeneroMusical genero;

	@NotNull
	@Column(name = "duracao_demo")
	private Integer duracaoDemo;

	@OneToOne(mappedBy = "faixa")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private FaixaFile file;
	
	@OneToOne(mappedBy = "faixa")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private FaixaFoto foto;

	@OneToMany(mappedBy = "faixa", fetch = FetchType.LAZY)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private List<FaixaLicenca> licencas = new ArrayList<>();
	
	@OneToMany(mappedBy = "faixa", fetch = FetchType.LAZY)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private List<LikeFaixa> likes = new ArrayList<>();

	public Faixa() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Artista getArtista() {
		return artista;
	}

	public void setArtista(Artista artista) {
		this.artista = artista;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDuracao() {
		return duracao;
	}

	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public GeneroMusical getGenero() {
		return genero;
	}

	public void setGenero(GeneroMusical genero) {
		this.genero = genero;
	}

	public Integer getDuracaoDemo() {
		return duracaoDemo;
	}

	public void setDuracaoDemo(Integer duracaoDemo) {
		this.duracaoDemo = duracaoDemo;
	}

	public FaixaFile getFile() {
		return file;
	}

	public void setFile(FaixaFile file) {
		this.file = file;
	}

	public FaixaFoto getFoto() {
		return foto;
	}

	public void setFoto(FaixaFoto foto) {
		this.foto = foto;
	}

	public List<FaixaLicenca> getLicencas() {
		return licencas;
	}

	public void setLicencas(List<FaixaLicenca> licencas) {
		this.licencas = licencas;
	}

	public List<LikeFaixa> getLikes() {
		return likes;
	}

	public void setLikes(List<LikeFaixa> likes) {
		this.likes = likes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Faixa other = (Faixa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ArtistaFaixa [id=" + id + "]";
	}

}
