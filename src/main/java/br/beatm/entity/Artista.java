package br.beatm.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

@Entity
@Table(name = "artista")
@DiscriminatorValue(value = "ARTISTA")
@Audited(withModifiedFlag = true)
public class Artista extends Usuario {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@NotNull
	private String nome;

	@NotNull
	@Column(name = "dt_nascimento")
	private Date dtNascimento;

	@NotNull
	private String telefone1;

	private String telefone2;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cidade_id")
	private Cidade cidade;

	@OneToOne(mappedBy = "artista")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private ArtistaFoto foto;

	@OneToMany(mappedBy = "artista", fetch = FetchType.LAZY)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private List<ArtistaGeneroMusical> generos = new ArrayList<>();

	@OneToMany(mappedBy = "artista", fetch = FetchType.LAZY)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private List<Faixa> faixas = new ArrayList<>();

	@OneToMany(mappedBy = "artista", fetch = FetchType.LAZY)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private List<Album> albuns = new ArrayList<>();

	@OneToMany(mappedBy = "artista", fetch = FetchType.LAZY)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private List<ArtistaSeguidor> seguidores = new ArrayList<>();

	@OneToMany(mappedBy = "artista", fetch = FetchType.LAZY)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag = false)
	private List<LikeArtista> likes = new ArrayList<>();

	public Artista() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public ArtistaFoto getFoto() {
		return foto;
	}

	public void setFoto(ArtistaFoto foto) {
		this.foto = foto;
	}

	public List<ArtistaGeneroMusical> getGeneros() {
		return generos;
	}

	public void setGeneros(List<ArtistaGeneroMusical> generos) {
		this.generos = generos;
	}

	public List<Faixa> getFaixas() {
		return faixas;
	}

	public void setFaixas(List<Faixa> faixas) {
		this.faixas = faixas;
	}

	public List<Album> getAlbuns() {
		return albuns;
	}

	public void setAlbuns(List<Album> albuns) {
		this.albuns = albuns;
	}

	public List<ArtistaSeguidor> getSeguidores() {
		return seguidores;
	}

	public void setSeguidores(List<ArtistaSeguidor> seguidores) {
		this.seguidores = seguidores;
	}

	public List<LikeArtista> getLikes() {
		return likes;
	}

	public void setLikes(List<LikeArtista> likes) {
		this.likes = likes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artista other = (Artista) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Artista [id=" + id + "]";
	}

}
