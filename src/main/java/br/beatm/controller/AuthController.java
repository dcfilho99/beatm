package br.beatm.controller;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.beatm.dto.UsuarioLogadoDTO;
import br.beatm.entity.Usuario;
import br.beatm.service.TokenAuthenticationService;
import br.beatm.service.UsuarioService;
import br.beatm.util.ToolsUtil;

@RestController
@RequestMapping("/auth")
public class AuthController extends BaseController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value = "/me", method = RequestMethod.POST)
	public ResponseEntity<Object> me() {
		if (TokenAuthenticationService.isTokenValido(request)) {
			Authentication auth = TokenAuthenticationService.getAuthentication(request);
			Usuario usuario = usuarioService.getUsuarioByEmail(auth.getPrincipal().toString());
			byte[] foto = usuarioService.getFotoUsuario(usuario, request);
			
			UsuarioLogadoDTO user = new UsuarioLogadoDTO();
			user.setUsuario(usuario);
			user.setFoto(Base64.getEncoder().encodeToString(foto));
			return new ResponseEntity<Object>(user, HttpStatus.OK);
		}
		return null;
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public ResponseEntity<Object> logout() {
		Authentication auth = TokenAuthenticationService.getAuthentication(request);
		if (!ToolsUtil.isNull(auth)) {
			auth.setAuthenticated(Boolean.FALSE);
			TokenAuthenticationService.clear(response);
		}
		return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
	}
}
