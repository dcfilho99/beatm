package br.beatm.config;

public interface IMensagemSistema {

	public final String GENERIC_ERROR = "[ERRO] Ocorreu um erro ao processar o pedido, tente novamente";
	public final String LOGIN_INCORRETO = "Dados incorretos";
	public final String SESSAO_EXPIROU = "Sua sessão expirou, faça o Login novamente para continuar!";
	public final String SEM_PERMISSAO = "Você não tem permissão para realizar a ação: ";
	public final String NENHUM_REGISTRO = "Nenhum registro encontrado!";
	public final String POSSUI_DEPENDENCIAS = "O Registro possui dependências, não pode ser excluído!"; 
	public final String DESLOGADO = "Você precisa logar no sistemas para continuar!";
	public final String REGISTRO_EXCLUIDO = "Registro excluído!";
	public final String REGISTRO_SALVO = "Registro salvo!";
	public final String RECURSO_NAO_ENCONTRADO = "A ação solicitada não existe, entre em contato com o suporte!";
	public final String REGISTRO_DUPLICADO_NOME = "Já existe um registro com esse NOME cadastrado: ";
	public final String USUARIO_DESATIVADO = "Usuário desativado!";
	public final String USUARIO_ATIVADO = "Usuário ativado!";
	public final String PRODUTO_NAO_ENCONTRADO = "Produto não encontrado";
	public final String VALOR_NAO_CADASTRADO = "Valor não cadastrado";
}

