package br.beatm.config;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.beatm.dto.RestResponseDto;
import br.beatm.util.JsonUtil;

@ControllerAdvice
@Component("exceptionHandlerControllerAdvice")
public class ExceptionHandlerControllerAdvice extends ResponseEntityExceptionHandler {

	
	@ExceptionHandler(value = UsernameNotFoundException.class)
	public @ResponseBody String authenticationFailed(UsernameNotFoundException e) {
		StringBuilder str = new StringBuilder();
		str.append(e.getMessage());
		
		RestResponseDto r = new RestResponseDto();
		r.setMensagem("<b style='font-size: 20px;'>" + str.toString() + "<b/>");
		r.setSuccess(Boolean.FALSE);
		r.setCode(401);
		return JsonUtil.toJSON(r);
	}
	
	/*@SuppressWarnings("rawtypes")
	@ExceptionHandler({ConstraintViolationException.class})
	public @ResponseBody ResponseEntity<Object> constraintViolation(Exception e, WebRequest request) {
		RestResponseDto resp = new RestResponseDto();

		StringBuilder str = new StringBuilder();
		ConstraintViolationException ce = (ConstraintViolationException) e;
		for (ConstraintViolation c : ce.getConstraintViolations()) {
			str.append("<b style='font-size: 20px;'>" + c.getMessage() + "</b><br/>");
		}

		resp.setSuccess(Boolean.FALSE);
		resp.setMensagem(str.toString());
		resp.setCode(200);
		resp.setData(null);

		return new ResponseEntity<Object>(resp, HttpStatus.OK);
	}
	
	@ExceptionHandler({org.springframework.dao.DataIntegrityViolationException.class})
	public @ResponseBody ResponseEntity<Object> dataIntegrityViolation(Exception e, WebRequest request) {
		RestResponseDto resp = new RestResponseDto();

		StringBuilder str = new StringBuilder();
		DataIntegrityViolationException ce = (DataIntegrityViolationException) e;
			
		if (!ToolsUtil.isNull(ce.getCause())) {
			if (ce.getCause().getCause().toString().contains("foreign key constraint")) {
				str.append("<b style='font-size: 20px;'>" + IMensagemSistema.POSSUI_DEPENDENCIAS + "</b>");
			}
		} else {
			str.append(ce.getMessage());
		}
		resp.setCode(200);
		resp.setSuccess(Boolean.FALSE);
		resp.setMensagem(str.toString());
		resp.setData(null);

		return new ResponseEntity<Object>(resp, HttpStatus.OK);
	}

	@ExceptionHandler(value = RuntimeException.class)
	public @ResponseBody ResponseEntity<Object> serviceException(Exception e, WebRequest request) {
		RestResponseDto resp = new RestResponseDto();

		StringBuilder str = new StringBuilder();

		str.append("<b style='font-size: 20px;'>" + IMensagemSistema.GENERIC_ERROR + "</b>");

		resp.setSuccess(Boolean.FALSE);
		resp.setMensagem(str.toString());
		resp.setData(null);
		resp.setCode(500);
		return new ResponseEntity<Object>(resp, HttpStatus.OK);
	}

	@ExceptionHandler(value = TransientPropertyValueException.class)
	public @ResponseBody ResponseEntity<Object> transientException(Exception e, WebRequest request) {
		RestResponseDto resp = new RestResponseDto();

		StringBuilder str = new StringBuilder();

		TransientPropertyValueException ex = (TransientPropertyValueException) e;

		str.append("<b style='font-size: 20px;'>" + ex.getMessage() + "</b>");

		resp.setSuccess(Boolean.FALSE);
		resp.setMensagem(str.toString());
		resp.setData(null);
		resp.setCode(200);

		return ResponseEntity.ok(resp);
	}

	@ExceptionHandler(value = UsernameNotFoundException.class)
	public @ResponseBody String authenticationFailed(UsernameNotFoundException e) {
		StringBuilder str = new StringBuilder();
		str.append(e.getMessage());
		
		RestResponseDto r = new RestResponseDto();
		r.setMensagem("<b style='font-size: 20px;'>" + str.toString() + "<b/>");
		r.setSuccess(Boolean.FALSE);
		r.setCode(401);
		return JsonUtil.toJSON(r);
	}
	
	@ExceptionHandler(value = PermissaoAcaoException.class)
	public @ResponseBody String permissaoAcaoException(PermissaoAcaoException e) {
		StringBuilder str = new StringBuilder();
		str.append(e.getMessage());
		
		RestResponseDto r = new RestResponseDto();
		r.setMensagem("<b style='font-size: 20px;'>" + str.toString() + "</b>");
		r.setSuccess(Boolean.FALSE);
		r.setCode(403);
		return JsonUtil.toJSON(r);
	}
	
	@ExceptionHandler(value = CampoObrigatorioException.class)
	public @ResponseBody String campoObrigaotorioException(CampoObrigatorioException e) {
		StringBuilder str = new StringBuilder();
		str.append(e.getMessage());
		
		RestResponseDto r = new RestResponseDto();
		r.setMensagem("<b style='font-size: 20px;'>" + str.toString() + "</b>");
		r.setSuccess(Boolean.FALSE);
		r.setCode(200);
		return JsonUtil.toJSON(r);
	}
	
	@ExceptionHandler(value = org.springframework.transaction.TransactionSystemException.class)
	public @ResponseBody Object transactionSystemException(TransactionSystemException e, WebRequest request) {
		if (e.getCause() instanceof ConstraintViolationException) {
			return this.constraintViolation((ConstraintViolationException) e.getCause(), request);
		} else if (e.getCause().getCause() instanceof ConstraintViolationException) {
			return this.constraintViolation((ConstraintViolationException) e.getCause().getCause(), request);
		} else {
			return this.serviceException(e, request);
		}
	}*/
}
