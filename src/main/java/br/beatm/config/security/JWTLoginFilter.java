package br.beatm.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.beatm.config.ExceptionHandlerControllerAdvice;
import br.beatm.config.IMensagemSistema;
import br.beatm.dto.AuthRequestDTO;
import br.beatm.entity.Usuario;
import br.beatm.exception.ServiceException;
import br.beatm.service.LoginService;
import br.beatm.service.TokenAuthenticationService;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {
	
	private LoginService loginService;
	
	private boolean isRememberMe;
	
	private ApplicationContext applicationContext;
	
	public JWTLoginFilter(String url, AuthenticationManager authManager, LoginService loginService, ApplicationContext applicationContext) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(authManager);
		this.loginService = loginService;
		this.applicationContext = applicationContext;
	}

	@CrossOrigin
	@Override
    public Authentication attemptAuthentication(
            HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException, IOException, ServletException {
		
		AuthRequestDTO creds = new ObjectMapper().readValue(req.getInputStream(), AuthRequestDTO.class);
		this.isRememberMe = creds.isRememberMe();
        return autenticarUsuario(req, res, creds);
    }

	@Transactional
	private Authentication autenticarUsuario(HttpServletRequest req, HttpServletResponse res, AuthRequestDTO creds) throws IOException {
		try {
			Usuario user = this.loginService.getUsuarioLogin(creds);
			if (user == null) {
				ExceptionHandlerControllerAdvice c = (ExceptionHandlerControllerAdvice) this.applicationContext.getBean("exceptionHandlerControllerAdvice");
				res.getOutputStream().write(c.authenticationFailed(new UsernameNotFoundException(IMensagemSistema.LOGIN_INCORRETO)).getBytes());
				return null;
			}
			ObjectMapper om = new ObjectMapper();
			String credentials = om.writeValueAsString(user);
			Authentication auth = new UsernamePasswordAuthenticationToken(user.getEmail(), credentials, null);   
        	return auth;
		} catch (ServiceException e) {
			ExceptionHandlerControllerAdvice c = (ExceptionHandlerControllerAdvice) this.applicationContext.getBean("exceptionHandlerControllerAdvice");
			res.getOutputStream().write(c.authenticationFailed(new UsernameNotFoundException(IMensagemSistema.LOGIN_INCORRETO)).getBytes());
		}
		return null;
    }

	@Override
    protected void successfulAuthentication(
            HttpServletRequest req,
            HttpServletResponse res, FilterChain chain,
            Authentication auth
    ) throws IOException, ServletException {
    	
    	TokenAuthenticationService.addAuthentication(res, auth.getPrincipal().toString(), isRememberMe);
    	SecurityContextHolder.getContext().setAuthentication(auth);
    }
}
