package br.beatm.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.beatm.service.LoginService;

@SuppressWarnings("deprecation")
@Configuration
@EnableWebSecurity
@ComponentScan("br.beatm")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private LoginService loginService;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
		.authorizeRequests()
		.antMatchers(HttpMethod.POST, "/auth/login").permitAll();
		/*.antMatchers("/assinador-digital/**").permitAll();*/
	
		httpSecurity
		// filtra requisições de login
		.addFilterBefore(new JWTLoginFilter("/auth/login", authenticationManager(), loginService, applicationContext),
                UsernamePasswordAuthenticationFilter.class)
		
		.csrf().disable()
		.cors();	
	}
	
	@Bean
	public static NoOpPasswordEncoder passwordEncoder() {
		return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
	}
}
